parser grammar Lang1Parser;

options { tokenVocab=Lang1Lexer; }

file : statement+ ;

statement : functionDefinition | functionInvocation ;

functionDefinition : DEF ID LPAREN idList RPAREN expression ;
functionInvocation : ID LPAREN expressionList RPAREN ;

idList : | ID | ID (COMMA ID)+ ;
expressionList : | expression | expression (COMMA expression)+ ;

expression : IF condition=expression conditionExpression=expression ELSE defaultExpression=expression # ifExpression
 | left=expression operator=(ASTERISK|DIVISION) right=expression # binaryExpression
 | left=expression operator=(PLUS|MINUS) right=expression # binaryExpression
 | left=expression operator=(LESSER|GREATER|EQUAL) right=expression # binaryExpression
 | functionInvocation # functionInvocationExpression
 | LPAREN expression RPAREN # parenthesisExpression
 | ID # identifierExpression
 | NUMLIT # numberLiteralExpression ;
