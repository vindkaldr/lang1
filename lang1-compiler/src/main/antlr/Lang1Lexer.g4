lexer grammar Lang1Lexer;

// Whitespace
NEWLINE : [\r\n]+ -> skip ;

WS : [\t ]+ -> skip ;

// Keywords
DEF : 'def' ;
IF : 'if' ;
ELSE : 'else' ;

// Literals
NUMLIT : '0' | '0.0' | [1-9][0-9]* | [1-9][0-9]*'.'[0-9]+ ;

// Operators
PLUS : '+' ;
MINUS : '-' ;
ASTERISK : '*' ;
DIVISION : '/' ;
LESSER : '<' ;
GREATER : '>' ;
EQUAL : '==' ;

// Punctuation marks
LPAREN : '(' ;
RPAREN : ')' ;
COMMA : ',' ;

// Identifiers
ID : [a-zA-Z][A-Za-z0-9]* ;
