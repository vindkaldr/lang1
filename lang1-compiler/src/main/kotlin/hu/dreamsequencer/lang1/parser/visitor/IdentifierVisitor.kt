/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.IdentifierExpressionContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.Identifier
import org.antlr.v4.runtime.tree.TerminalNode

class IdentifierVisitor : Lang1ParserBaseVisitor<Identifier>() {
    override fun visitIdentifierExpression(ctx: IdentifierExpressionContext): Identifier {
        requireNotNull(ctx)

        val name = getName(ctx)
        val position = getPosition(ctx)
        return Identifier(name, position)
    }

    private fun getName(context: IdentifierExpressionContext): String {
        requireNotNull(context.ID())
        return context.ID().text
    }

    override fun visitTerminal(node: TerminalNode): Identifier {
        requireNotNull(node)

        val name = getName(node)
        val position = getPosition(node)
        return Identifier(name, position)
    }

    private fun getName(node: TerminalNode): String {
        requireNotNull(node.text)
        return node.text
    }
}
