/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.BinaryExpressionContext
import hu.dreamsequencer.lang1.Lang1Parser.ExpressionContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.BinaryExpression
import hu.dreamsequencer.lang1.parser.Expression

class BinaryExpressionVisitor : Lang1ParserBaseVisitor<BinaryExpression>() {
    private companion object {
        val expressionVisitor = ExpressionVisitor()
    }

    override fun visitBinaryExpression(ctx: BinaryExpressionContext): BinaryExpression {
        requireNotNull(ctx)

        val left = getLeftExpression(ctx)
        val operator = getOperator(ctx)
        val right = getRightExpression(ctx)
        val position = getPosition(ctx)

        return BinaryExpression(left, operator, right, position)
    }

    private fun getLeftExpression(context: BinaryExpressionContext): Expression {
        requireNotNull(context.left)
        return getExpression(context.left)
    }

    private fun getRightExpression(context: BinaryExpressionContext): Expression {
        requireNotNull(context.right)
        return getExpression(context.right)
    }

    private fun getExpression(context: ExpressionContext): Expression {
        return expressionVisitor.visit(context)
    }

    private fun getOperator(context: BinaryExpressionContext): String {
        requireNotNull(context.operator?.text)
        return context.operator.text
    }
}
