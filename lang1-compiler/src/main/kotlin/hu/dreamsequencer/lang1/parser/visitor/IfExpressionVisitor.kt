/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.ExpressionContext
import hu.dreamsequencer.lang1.Lang1Parser.IfExpressionContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.EmptyExpression
import hu.dreamsequencer.lang1.parser.Expression
import hu.dreamsequencer.lang1.parser.IfExpression

class IfExpressionVisitor : Lang1ParserBaseVisitor<IfExpression>() {
    private companion object {
        val expressionVisitor = ExpressionVisitor()
    }

    override fun visitIfExpression(ctx: IfExpressionContext): IfExpression {
        requireNotNull(ctx)

        val condition = getCondition(ctx)
        val expression = getConditionExpression(ctx)
        val defaultExpression = getDefaultExpression(ctx)
        val position = getPosition(ctx)

        return IfExpression(condition, expression, defaultExpression, position)
    }

    private fun getCondition(context: IfExpressionContext): Expression {
        requireNotNull(context.condition)
        return getExpression(context.condition)
    }

    private fun getConditionExpression(context: IfExpressionContext): Expression {
        requireNotNull(context.conditionExpression)
        return getExpression(context.conditionExpression)
    }

    private fun getDefaultExpression(context: IfExpressionContext): Expression {
        if (context.defaultExpression == null) return EmptyExpression
        return getExpression(context.defaultExpression)
    }

    private fun getExpression(context: ExpressionContext): Expression {
        return expressionVisitor.visit(context)
    }
}
