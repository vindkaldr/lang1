/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.FunctionDefinitionContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.EmptyExpression
import hu.dreamsequencer.lang1.parser.Expression
import hu.dreamsequencer.lang1.parser.FunctionDefinition
import hu.dreamsequencer.lang1.parser.Identifier

class FunctionDefinitionVisitor : Lang1ParserBaseVisitor<FunctionDefinition>() {
    private companion object {
        val expressionVisitor = ExpressionVisitor()
        val identifierVisitor = IdentifierVisitor()
    }

    override fun visitFunctionDefinition(ctx: FunctionDefinitionContext): FunctionDefinition {
        requireNotNull(ctx)

        val id = getId(ctx)
        val arguments = getArguments(ctx)
        val expression = getExpression(ctx)
        val position = getPosition(ctx)
        return FunctionDefinition(id, arguments, expression, position)
    }

    private fun getId(ctx: FunctionDefinitionContext): Identifier {
        requireNotNull(ctx.ID())
        return identifierVisitor.visitTerminal(ctx.ID())
    }

    private fun getArguments(context: FunctionDefinitionContext): List<Identifier> {
        val idList = context.idList()?.ID() ?: return emptyList()
        return idList.map { identifierVisitor.visitTerminal(it) }
    }

    private fun getExpression(ctx: FunctionDefinitionContext): Expression {
        if (ctx.expression() == null) return EmptyExpression
        return expressionVisitor.visit(ctx.expression())
    }
}
