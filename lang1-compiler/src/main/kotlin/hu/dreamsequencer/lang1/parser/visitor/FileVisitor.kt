/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.FileContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.File
import hu.dreamsequencer.lang1.parser.Statement

class FileVisitor : Lang1ParserBaseVisitor<File>() {
    private companion object {
        val statementVisitor = StatementVisitor()
    }

    override fun visitFile(ctx: FileContext): File {
        requireNotNull(ctx)
        val statements = getStatements(ctx)
        return File(statements)
    }

    private fun getStatements(ctx: FileContext): List<Statement> {
        return ctx.statement()
                ?.map { statementVisitor.visit(it) }
                .orEmpty()
    }
}

