/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.FunctionInvocationContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.Expression
import hu.dreamsequencer.lang1.parser.FunctionInvocation
import hu.dreamsequencer.lang1.parser.Identifier

class FunctionInvocationVisitor : Lang1ParserBaseVisitor<FunctionInvocation>() {
    private companion object {
        val expressionVisitor = ExpressionVisitor()
        val identifierVisitor = IdentifierVisitor()
    }

    override fun visitFunctionInvocation(ctx: FunctionInvocationContext): FunctionInvocation {
        requireNotNull(ctx)

        val id = getId(ctx)
        val parameters = getParameters(ctx)
        val position = getPosition(ctx)
        return FunctionInvocation(id, parameters, position)
    }

    private fun getId(ctx: FunctionInvocationContext): Identifier {
        requireNotNull(ctx.ID())
        return identifierVisitor.visitTerminal(ctx.ID())
    }

    private fun getParameters(ctx: FunctionInvocationContext): List<Expression> {
        val parameters = ctx.expressionList()?.expression() ?: return emptyList()
        return parameters.map { expressionVisitor.visit(it) }
    }
}
