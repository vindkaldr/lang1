/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

data class File(val statements: List<Statement>)

interface Statement
interface Expression
object EmptyExpression : Expression

data class FunctionDefinition(
        val id: Identifier,
        val arguments: List<Identifier>,
        val expression: Expression,
        val position: Position
) : Statement

data class FunctionInvocation(
        val id: Identifier,
        val parameters: List<Expression>,
        val position: Position
) : Statement, Expression

data class BinaryExpression(
        val leftSide: Expression,
        val operator: String,
        val rightSide: Expression,
        val position: Position
) : Expression

data class IfExpression(
        val condition: Expression,
        val conditionExpression: Expression,
        val defaultExpression: Expression,
        val position: Position
) : Expression

data class Identifier(val name: String, val position: Position) : Expression
data class NumberLiteral(val value: String, val position: Position) : Expression

data class Position(val line: Int, val characterPosition: Int)
