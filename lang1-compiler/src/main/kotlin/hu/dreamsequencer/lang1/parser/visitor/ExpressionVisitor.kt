/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser
import hu.dreamsequencer.lang1.Lang1Parser.BinaryExpressionContext
import hu.dreamsequencer.lang1.Lang1Parser.FunctionInvocationContext
import hu.dreamsequencer.lang1.Lang1Parser.IdentifierExpressionContext
import hu.dreamsequencer.lang1.Lang1Parser.NumberLiteralExpressionContext
import hu.dreamsequencer.lang1.Lang1Parser.ParenthesisExpressionContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.EmptyExpression
import hu.dreamsequencer.lang1.parser.Expression

class ExpressionVisitor : Lang1ParserBaseVisitor<Expression>() {
    private companion object {
        val functionInvocationVisitor = FunctionInvocationVisitor()
        val ifExpressionVisitor = IfExpressionVisitor()
        val binaryExpressionVisitor = BinaryExpressionVisitor()
        val parenthesisVisitor = ParenthesisVisitor()
        val identifierVisitor = IdentifierVisitor()
        val numberLiteralVisitor = NumberLiteralVisitor()
    }

    override fun visitFunctionInvocation(ctx: FunctionInvocationContext): Expression {
        requireNotNull(ctx)
        return functionInvocationVisitor.visit(ctx)
    }

    override fun visitIfExpression(ctx: Lang1Parser.IfExpressionContext?): Expression {
        requireNotNull(ctx)
        return ifExpressionVisitor.visit(ctx)
    }

    override fun visitBinaryExpression(ctx: BinaryExpressionContext): Expression {
        requireNotNull(ctx)
        return binaryExpressionVisitor.visit(ctx)
    }

    override fun visitParenthesisExpression(ctx: ParenthesisExpressionContext): Expression {
        requireNotNull(ctx)
        return parenthesisVisitor.visit(ctx)
    }

    override fun visitIdentifierExpression(ctx: IdentifierExpressionContext): Expression {
        requireNotNull(ctx)
        return identifierVisitor.visit(ctx)
    }

    override fun visitNumberLiteralExpression(ctx: NumberLiteralExpressionContext): Expression {
        requireNotNull(ctx)
        return numberLiteralVisitor.visit(ctx)
    }

    override fun defaultResult() = EmptyExpression
}
