/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser.visitor

import hu.dreamsequencer.lang1.Lang1Parser.FunctionDefinitionContext
import hu.dreamsequencer.lang1.Lang1Parser.FunctionInvocationContext
import hu.dreamsequencer.lang1.Lang1ParserBaseVisitor
import hu.dreamsequencer.lang1.parser.Statement

class StatementVisitor : Lang1ParserBaseVisitor<Statement>() {
    private companion object {
        val functionDefinitionVisitor = FunctionDefinitionVisitor()
        val functionInvocationVisitor = FunctionInvocationVisitor()
    }

    override fun visitFunctionDefinition(ctx: FunctionDefinitionContext): Statement {
        requireNotNull(ctx)
        return functionDefinitionVisitor.visit(ctx)
    }

    override fun visitFunctionInvocation(ctx: FunctionInvocationContext): Statement {
        requireNotNull(ctx)
        return functionInvocationVisitor.visit(ctx)
    }
}
