/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.lexer

data class LexerToken(val token: String, val value: String)
data class LexerError(val line: Int, val characterPosition: Int, val message: String)

val def = token("DEF")
val `if` = token("IF")
val `else` = token("ELSE")

val plus = token("PLUS")
val minus = token("MINUS")
val asterisk = token("ASTERISK")
val division = token("DIVISION")
val lesser = token("LESSER")
val greater = token("GREATER")
val equal = token("EQUAL")

val lparen = token("LPAREN")
val rparen = token("RPAREN")
val comma = token("COMMA")

fun number(value: Number) = number(value.toString())
fun number(value: String) = token("NUMLIT", value)
fun id(name: String) = token("ID", name)

fun token(token: String) = token(token, "")
fun token(token: String, value: String) = LexerToken(token, value)
fun error(line: Int, characterPosition: Int) = LexerError(line, characterPosition, "")
