/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.lexer

import org.junit.jupiter.api.Test

class LexerErrorTest {
    @Test
    fun `finds illegal character`() {
        assertTokens(
                code = "? def",
                tokens = listOf(def),
                errors = listOf(error(line = 1, characterPosition = 0)))
    }

    @Test
    fun `finds illegal characters`() {
        assertTokens(
                code = "?á def @",
                tokens = listOf(def),
                errors = listOf(
                        error(line = 1, characterPosition = 0),
                        error(line = 1, characterPosition = 1),
                        error(line = 1, characterPosition = 7)))
    }

    @Test
    fun `finds illegal characters across lines`() {
        assertTokens(
                code = """á
                    |é def í() ó
                    |ő    1 ú ű
                """.trimMargin(),
                tokens = listOf(def, lparen, rparen, number(1)),
                errors = listOf(
                        error(line = 1, characterPosition = 0),
                        error(line = 2, characterPosition = 0),
                        error(line = 2, characterPosition = 6),
                        error(line = 2, characterPosition = 10),
                        error(line = 3, characterPosition = 0),
                        error(line = 3, characterPosition = 7),
                        error(line = 3, characterPosition = 9)))
    }
}
