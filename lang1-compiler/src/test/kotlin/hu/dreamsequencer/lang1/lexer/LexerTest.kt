/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.lexer

import org.junit.jupiter.api.Test

class LexerTest {
    @Test
    fun `scans empty code`() {
        assertTokens(code = "", tokens = emptyList())
    }

    @Test
    fun `scans keywords`() {
        assertTokens(code = "def if else", tokens = listOf(def, `if`, `else`))
    }

    @Test
    fun `scans number literals`() {
        assertToken(code = "0", token = number(0))
        assertToken(code = "0.0", token = number(0.0))
        assertToken(code = "1", token = number(1))
        assertToken(code = "1.0", token = number(1.0))
        assertToken(code = "1.1", token = number(1.1))
        assertToken(code = "1.11", token = number(1.11))
        assertToken(code = "11", token = number(11))
        assertToken(code = "11.0", token = number(11.0))
        assertToken(code = "11.1", token = number(11.1))
        assertToken(code = "11.11", token = number(11.11))
        assertToken(code = "9", token = number(9))
        assertToken(code = "9.0", token = number(9.0))
        assertToken(code = "9.9", token = number(9.9))
        assertToken(code = "9.99", token = number(9.99))
    }

    @Test
    fun `scans operators`() {
        assertTokens(
                code = "+ - * / < > ==",
                tokens = listOf(plus, minus, asterisk, division, lesser, greater, equal))
    }

    @Test
    fun `scans punctuation marks`() {
        assertTokens(
                code = "((())) ,",
                tokens = listOf(lparen, lparen, lparen, rparen, rparen, rparen, comma))
    }

    @Test
    fun `scans identifier`() {
        assertToken(code = "a", token = id("a"))
        assertToken(code = "ab", token = id("ab"))
        assertToken(code = "abc", token = id("abc"))
        assertToken(code = "a1", token = id("a1"))
        assertToken(code = "a1b", token = id("a1b"))
        assertToken(code = "ab1", token = id("ab1"))
        assertToken(code = "A1", token = id("A1"))
        assertToken(code = "A1B", token = id("A1B"))
        assertToken(code = "AB1", token = id("AB1"))
    }

    @Test
    fun `scans number expressions`() {
        assertTokens(code = "1 + 2", tokens = listOf(number(1), plus,  number(2)))
        assertTokens(code = "1 - 2", tokens = listOf(number(1), minus, number(2)))
        assertTokens(code = "1 * 2", tokens = listOf(number(1), asterisk, number(2)))
        assertTokens(code = "1 / 2", tokens = listOf( number(1), division, number(2)))

        assertTokens(code = "x + 2", tokens = listOf(id("x"), plus, number(2)))
        assertTokens(code = "x + 2 + 3", tokens = listOf(id("x"), plus,  number(2), plus, number(3)))

        assertTokens(
                code = "((1) + (2))",
                tokens = listOf(lparen, lparen, number(1), rparen, plus, lparen, number(2), rparen, rparen))
    }

    @Test
    fun `scans boolean expressions`() {
        assertTokens(code = "1 < 3", tokens = listOf(number(1), lesser, number(3)))
        assertTokens(code = "1 > 3", tokens = listOf(number(1), greater, number(3)))
        assertTokens(code = "1 == 3", tokens = listOf(number(1), equal, number(3)))

        assertTokens(code = "x < 3", tokens = listOf(id("x"), lesser, number(3)))
        assertTokens(code = "x < 3 - 1", tokens = listOf(id("x"), lesser, number(3), minus, number(1)))

        assertTokens(
                code = "((1) < (3))",
                tokens = listOf(lparen, lparen, number(1), rparen, lesser, lparen, number(3), rparen, rparen))
    }

    @Test
    fun `scans function definitions`() {
        assertTokens(code = "def a() 1", tokens = listOf(def, id("a"), lparen, rparen, number(1)))
        assertTokens(code = "def a(x) 1", tokens = listOf(def, id("a"), lparen, id("x"), rparen, number(1)))

        assertTokens(
                code = """
                    def a(x, y, z)
                        x + 1 + z
                """,
                tokens = listOf(
                    def, id("a"), lparen, id("x"), comma, id("y"), comma, id("z"), rparen,
                        id("x"), plus, number(1), plus, id("z")))

        assertTokens(
                code = """
                    def a(x, y)
                        b(x, 1, 1 + y)
                """,
                tokens = listOf(
                    def, id("a"), lparen, id("x"), comma, id("y"), rparen,
                        id("b"), lparen, id("x"), comma, number(1), comma, number(1), plus, id("y"), rparen))
    }

    @Test
    fun `scans function invocations`() {
        assertTokens(code = "a()", tokens = listOf(id("a"), lparen, rparen))
        assertTokens(code = "a(1)", tokens = listOf(id("a"), lparen, number(1), rparen))
        assertTokens(code = "a(x)", tokens = listOf(id("a"), lparen, id("x"), rparen))
        assertTokens(code = "a(1, x)", tokens = listOf(id("a"), lparen, number(1), comma, id("x"), rparen))
        assertTokens(code = "a(x, 1)", tokens = listOf(id("a"), lparen, id("x"), comma, number(1), rparen))

        assertTokens(code = "a(1 + x)", tokens = listOf(id("a"), lparen, number(1), plus, id("x"), rparen))
        assertTokens(code = "a(x + 1)", tokens = listOf(id("a"), lparen, id("x"), plus, number(1), rparen))
        assertTokens(
                code = "a(1 + x, x + 1)",
                tokens = listOf(id("a"), lparen, number(1), plus, id("x"), comma, id("x"), plus, number(1), rparen))

        assertTokens(
                code = "a(1, 2, 3)",
                tokens = listOf(id("a"), lparen, number(1), comma, number(2), comma, number(3), rparen))
        assertTokens(
                code = "a(x, y, z)",
                tokens = listOf(id("a"), lparen, id("x"), comma, id("y"), comma, id("z"), rparen))
    }

    @Test
    fun `scans function definition and function invocation`() {
        assertTokens(
                code = """
                    def fib(x)
                      if x < 3
                        1
                      else
                        (fib(x - 1) + fib(x - 2))

                    fib(40)
                """,
                tokens = listOf(
                    def, id("fib"), lparen, id("x"), rparen,
                        `if`, id("x"), lesser, number(3),
                            number(1),
                        `else`,
                            lparen, id("fib"), lparen, id("x"), minus, number(1), rparen,
                                plus, id("fib"), lparen, id("x"), minus, number(2), rparen, rparen,
                    id("fib"), lparen, number(40), rparen))
    }
}
