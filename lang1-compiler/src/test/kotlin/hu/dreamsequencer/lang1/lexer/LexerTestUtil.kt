/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.lexer

import hu.dreamsequencer.lang1.Lang1Lexer
import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.BaseErrorListener
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.Vocabulary
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import java.io.StringReader

fun assertToken(code: String, token: LexerToken) {
    assertTokens(code, listOf(token))
}

fun assertTokens(code: String, tokens: List<LexerToken>, errors: List<LexerError> = emptyList()) {
    val configuration = getLexerConfiguration(code)
    assertThat(getLexerTokens(configuration.lexer), equalTo(tokens))
    assertErrors(configuration.errors, errors)
}

private fun getLexerConfiguration(code: String): LexerConfiguration {
    val errorListener = getLexerErrorListener()
    return LexerConfiguration(getLexer(code, errorListener), errorListener)
}

private fun getLexerErrorListener() = LexerErrorListener()

private fun getLexer(code: String, errorListener: LexerErrorListener): Lang1Lexer {
    return Lang1Lexer(ANTLRInputStream(StringReader(code))).apply {
        removeErrorListeners()
        addErrorListener(errorListener)
    }
}

private fun getLexerTokens(lexer: Lang1Lexer): List<LexerToken> {
    return generateSequence { lexer.nextToken() }
            .takeWhile { it.type != Token.EOF }
            .map { getLexerToken(lexer.vocabulary, it) }
            .toList()
}

private fun getLexerToken(vocabulary: Vocabulary, token: Token): LexerToken {
    return when (token.type) {
        Lang1Lexer.NUMLIT -> number(token.text)
        Lang1Lexer.ID -> id(token.text)
        else -> token(vocabulary.getSymbolicName(token.type))
    }
}

private fun assertErrors(actualErrors: List<LexerError>, expectedErrors: List<LexerError>) {
    assertThat(actualErrors.size, equalTo(expectedErrors.size))
    actualErrors.zip(expectedErrors).forEach { assertError(it.first, it.second) }
}

private fun assertError(actualError: LexerError, expectedError: LexerError) {
    assertThat(actualError.line, equalTo(expectedError.line))
    assertThat(actualError.characterPosition, equalTo(expectedError.characterPosition))
}

private class LexerConfiguration(val lexer: Lang1Lexer, errorListener: LexerErrorListener) {
    val errors: List<LexerError> = errorListener.errors
}

private class LexerErrorListener : BaseErrorListener() {
    val errors = mutableListOf<LexerError>()

    override fun syntaxError(recognizer: Recognizer<*, *>?, offendingSymbol: Any?,
            line: Int, charPositionInLine: Int, msg: String,
            e: RecognitionException?) {
        requireNotNull(line)
        requireNotNull(charPositionInLine)
        requireNotNull(msg)
        errors += LexerError(line, charPositionInLine, msg)
    }
}
