/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

fun file(block: FileBuilder.() -> Unit) = FileBuilder().apply(block).build()

@DslMarker
annotation class ParserModelDslMarker

@ParserModelDslMarker
class FileBuilder {
    private val statements = mutableListOf<Statement>()

    fun functionDefinition(position: Position, block: FunctionDefinitionBuilder.() -> Unit) {
        statements += FunctionDefinitionBuilder(position).apply(block).build()
    }

    fun functionInvocation(position: Position, block: FunctionInvocationBuilder.() -> Unit = {}) {
        statements += FunctionInvocationBuilder(position).apply(block).build()
    }

    fun build() = File(statements)
}

@ParserModelDslMarker
class FunctionDefinitionBuilder(private val position: Position) {
    private lateinit var id: Identifier
    private val arguments = mutableListOf<Identifier>()
    private lateinit var expression: Expression

    fun id(id: String, position: Position) {
        id(identifier(id, position))
    }

    fun id(identifier: Identifier) {
        this.id = identifier
    }

    fun argument(identifier: String, position: Position) {
        argument(identifier(identifier, position))
    }

    fun argument(identifier: Identifier) {
        this.arguments += identifier
    }

    fun returns(number: Int, position: Position) {
        this.expression = number(number, position)
    }

    fun returns(expression: () -> Expression) {
        returns(expression())
    }

    fun returns(expression: Expression) {
        this.expression = expression
    }

    fun functionInvocation(position: Position, block: FunctionInvocationBuilder.() -> Unit): FunctionInvocation {
        return FunctionInvocationBuilder(position).apply(block).build()
    }

    fun ifExpression(position: Position, block: IfExpressionBuilder.() -> Unit): IfExpression {
        return IfExpressionBuilder(position).apply(block).build()
    }

    fun binaryExpression(position: Position, block: BinaryExpressionBuilder.() -> Unit): BinaryExpression {
        return BinaryExpressionBuilder(position).apply(block).build()
    }

    fun build() = FunctionDefinition(id, arguments, expression, position)
}

@ParserModelDslMarker
class FunctionInvocationBuilder(private val position: Position) {
    private lateinit var id: Identifier
    private val parameters = mutableListOf<Expression>()

    fun id(identifier: String, position: Position) {
        this.id = identifier(identifier, position)
    }

    fun parameter(identifier: String, position: Position) {
        parameter(identifier(identifier, position))
    }

    fun parameter(number: Int, position: Position) {
        parameter(number(number, position))
    }

    fun parameter(expression: () -> Expression) {
        parameter(expression())
    }

    fun parameter(expression: Expression) {
        this.parameters += expression
    }

    fun binaryExpression(position: Position, block: BinaryExpressionBuilder.() -> Unit): BinaryExpression {
        return BinaryExpressionBuilder(position).apply(block).build()
    }

    fun build() = FunctionInvocation(id, parameters, position)
}

@ParserModelDslMarker
class BinaryExpressionBuilder(private val position: Position) {
    private lateinit var left: Expression
    private lateinit var operator: String
    private lateinit var right: Expression

    fun left(identifier: String, position: Position) {
        left(identifier(identifier, position))
    }

    fun left(number: Int, position: Position) {
        left(number(number, position))
    }

    fun left(expression: () -> Expression) {
        left(expression())
    }

    private fun left(expression: Expression) {
        left = expression
    }

    fun operator(operator: String) {
        this.operator = operator
    }

    fun right(identifier: String, position: Position) {
        right(identifier(identifier, position))
    }

    fun right(number: Int, position: Position) {
        right(number(number, position))
    }

    fun right(expression: () -> Expression) {
        right(expression())
    }

    private fun right(expression: Expression) {
        right = expression
    }

    fun functionInvocation(position: Position, block: FunctionInvocationBuilder.() -> Unit): FunctionInvocation {
        return FunctionInvocationBuilder(position).apply(block).build()
    }

    fun binaryExpression(position: Position, block: BinaryExpressionBuilder.() -> Unit): BinaryExpression {
        return BinaryExpressionBuilder(position).apply(block).build()
    }

    fun build() = BinaryExpression(left, operator, right, position)
}

class IfExpressionBuilder(private val position: Position) {
    private lateinit var condition: Expression
    private lateinit var conditionExpression: Expression
    private lateinit var defaultExpression: Expression

    fun condition(expression: () -> Expression) {
        condition(expression())
    }

    fun condition(expression: Expression) {
        this.condition = expression
    }

    fun conditionExpression(number: Int, position: Position) {
        conditionExpression(number(number, position))
    }

    fun conditionExpression(expression: () -> Expression) {
        conditionExpression(expression())
    }

    private fun conditionExpression(expression: Expression) {
        this.conditionExpression = expression
    }

    fun defaultExpression(number: Int, position: Position) {
        defaultExpression(number(number, position))
    }

    fun defaultExpression(expression: () -> Expression) {
        defaultExpression(expression())
    }

    fun defaultExpression(defaultExpression: Expression) {
        this.defaultExpression = defaultExpression
    }

    fun functionInvocation(position: Position, block: FunctionInvocationBuilder.() -> Unit): FunctionInvocation {
        return FunctionInvocationBuilder(position).apply(block).build()
    }

    fun build() = IfExpression(condition, conditionExpression, defaultExpression, position)
}

fun identifier(name: String, position: Position) = Identifier(name, position)
fun number(value: Int, position: Position) = NumberLiteral(value.toString(), position)
fun position(line: Int, characterPosition: Int) = Position(line, characterPosition)
