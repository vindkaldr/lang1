/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import org.junit.jupiter.api.Test

class FunctionDefinitionParserTest {
    @Test
    fun `parses function definition with no argument`() {
        assertModel(
                code = "def a() 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        returns(1, position(1, 8))
                    }
                })
    }

    @Test
    fun `parses function definition with one argument`() {
        assertModel(
                code = "def a(x) 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns(1, position(1, 9))
                    }
                })
    }

    @Test
    fun `parses function definition with more than one argument`() {
        assertModel(
                code = "def a(x, y, z) 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        argument("y", position(1, 9))
                        argument("z", position(1, 12))
                        returns(1, position(1, 15))
                    }
                })
    }

    @Test
    fun `parses function definition with number literal expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) 1 $operator 2",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                binaryExpression(position(1, 9)) {
                                    left(1, position(1, 9))
                                    operator(operator)
                                    right(2, position(1, 13))
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function definition with number literal and identifier expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) 1 $operator x",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                binaryExpression(position(1, 9)) {
                                    left(1, position(1, 9))
                                    operator(operator)
                                    right("x", position(1, 13))
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function definition with identifier and number literal expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) x $operator 1",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                binaryExpression(position(1, 9)) {
                                    left("x", position(1, 9))
                                    operator(operator)
                                    right(1, position(1, 13))
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function definition with function invocation and number literal`() {
        assertModel(
                code = "def a(x) b(1)",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            functionInvocation(position(1, 9)) {
                                id("b", position(1, 9))
                                parameter(1, position(1, 11))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses function definition with function invocation and number literal expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) b(1 $operator 2)",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                functionInvocation(position(1, 9)) {
                                    id("b", position(1, 9))
                                    parameter {
                                        binaryExpression(position(1, 11)) {
                                            left(1, position(1, 11))
                                            operator(operator)
                                            right(2, position(1, 15))
                                        }
                                    }
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function definition with function invocation, number literal and identifier expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) b(1 $operator x)",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                functionInvocation(position(1, 9)) {
                                    id("b", position(1, 9))
                                    parameter {
                                        binaryExpression(position(1, 11)) {
                                            left(1, position(1, 11))
                                            operator(operator)
                                            right("x", position(1, 15))
                                        }
                                    }
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function definition with function invocation and identifier`() {
        assertModel(
                code = "def a(x) b(x)",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            functionInvocation(position(1, 9)) {
                                id("b", position(1, 9))
                                parameter("x", position(1, 11))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses function definition with function invocation and identifier expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) b(x $operator x)",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                functionInvocation(position(1, 9)) {
                                    id("b", position(1, 9))
                                    parameter {
                                        binaryExpression(position(1, 11)) {
                                            left("x", position(1, 11))
                                            operator(operator)
                                            right("x", position(1, 15))
                                        }
                                    }
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function definition with function invocation, identifier and number literal expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "def a(x) b(x $operator 1)",
                    model = file {
                        functionDefinition(position(1, 0)) {
                            id("a", position(1, 4))
                            argument("x", position(1, 6))
                            returns {
                                functionInvocation(position(1, 9)) {
                                    id("b", position(1, 9))
                                    parameter {
                                        binaryExpression(position(1, 11)) {
                                            left("x", position(1, 11))
                                            operator(operator)
                                            right(1, position(1, 15))
                                        }
                                    }
                                }
                            }
                        }
                    })
        }
    }
}
