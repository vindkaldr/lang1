/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import org.junit.jupiter.api.Test

class FunctionInvocationParserTest {
    @Test
    fun `parses function invocation with no parameter`() {
        assertModel(
                code = "a()",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                    }
                })
    }

    @Test
    fun `parses function invocation with number literal`() {
        assertModel(
                code = "a(1)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                    }
                })
    }

    @Test
    fun `parses function invocation with more than one number literal`() {
        assertModel(
                code = "a(1, 2, 3)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                        parameter(2, position(1, 5))
                        parameter(3, position(1, 8))
                    }
                })
    }

    @Test
    fun `parses function invocation with number literal expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "a(1 $operator 2)",
                    model = file {
                        functionInvocation(position(1, 0)) {
                            id("a", position(1, 0))
                            parameter {
                                binaryExpression(position(1, 2)) {
                                    left(1, position(1, 2))
                                    operator(operator)
                                    right(2, position(1, 6))
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function invocation with more than one number literal expression`() {
        assertModel(
                code = "a(1 - 2, 3 + 4)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter {
                            binaryExpression(position(1, 2)) {
                                left(1, position(1, 2))
                                operator("-")
                                right(2, position(1, 6))
                            }
                        }
                        parameter {
                            binaryExpression(position(1, 9)) {
                                left(3, position(1, 9))
                                operator("+")
                                right(4, position(1, 13))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses function invocation with identifier`() {
        assertModel(
                code = "a(x)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter("x", position(1, 2))
                    }
                })
    }

    @Test
    fun `parses function invocation with more than one identifier`() {
        assertModel(
                code = "a(x, y, z)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter("x", position(1, 2))
                        parameter("y", position(1, 5))
                        parameter("z", position(1, 8))
                    }
                })
    }

    @Test
    fun `parses function invocation with identifier expression`() {
        listOf("-", "+", "*", "/").forEach { operator ->
            assertModel(
                    code = "a(x $operator y)",
                    model = file {
                        functionInvocation(position(1, 0)) {
                            id("a", position(1, 0))
                            parameter {
                                binaryExpression(position(1, 2)) {
                                    left("x", position(1, 2))
                                    operator(operator)
                                    right("y", position(1, 6))
                                }
                            }
                        }
                    })
        }
    }

    @Test
    fun `parses function invocation with more than one identifier expression`() {
        assertModel(
                code = "a(x - y, z + w)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter {
                            binaryExpression(position(1, 2)) {
                                left("x", position(1, 2))
                                operator("-")
                                right("y", position(1, 6))
                            }
                        }
                        parameter {
                            binaryExpression(position(1, 9)) {
                                left("z", position(1, 9))
                                operator("+")
                                right("w", position(1, 13))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses function invocation with number, identifier, number expression and identifier expression`() {
        assertModel(
                code = "a(1, x, 3 + 4, y + z)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                        parameter("x", position(1, 5))
                        parameter {
                            binaryExpression(position(1, 8)) {
                                left(3, position(1, 8))
                                operator("+")
                                right(4, position(1, 12))
                            }
                        }
                        parameter {
                            binaryExpression(position(1, 15)) {
                                left("y", position(1, 15))
                                operator("+")
                                right("z", position(1, 19))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses function invocation with parenthesis`() {
        assertModel(
                code = "a((1), (x), ((((3)) + (4))), ((y) + (z)))",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 3))
                        parameter("x", position(1, 8))
                        parameter {
                            binaryExpression(position(1, 14)) {
                                left(3, position(1, 16))
                                operator("+")
                                right(4, position(1, 23))
                            }
                        }
                        parameter {
                            binaryExpression(position(1, 30)) {
                                left("y", position(1, 31))
                                operator("+")
                                right("z", position(1, 37))
                            }
                        }
                    }
                })
    }
}
