/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import org.junit.jupiter.api.Test

class IfExpressionExpressionParserErrorTest {
    @Test
    fun `reports missing condition expression`() {
        assertModel(
                code = """
                    |def a(x)
                    |    if x == 0
                    |
                    |    else
                    |        1
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left("x", position(2, 7))
                                        operator("==")
                                        right(0, position(2, 12))
                                    }
                                }
                                conditionExpression(1, position(5, 8))
                                defaultExpression(missingExpression())
                            }
                        }
                    }
                },
                errors = positions {
                    position(4, 4)
                    position(5, 9)
                })
    }

    @Test
    fun `reports missing default expression`() {
        assertModel(
                code = """
                    |def a(x)
                    |    if x == 0
                    |        0
                    |    else
                    |
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left("x", position(2, 7))
                                        operator("==")
                                        right(0, position(2, 12))
                                    }
                                }
                                conditionExpression(0, position(3, 8))
                                defaultExpression(missingExpression())
                            }
                        }
                    }
                },
                error = position(5, 0))
    }
}
