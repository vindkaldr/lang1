/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import org.junit.jupiter.api.Test

class FunctionDefinitionParserErrorTest {
    @Test
    fun `reports missing name`() {
        assertModel(
                code = "def () 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id(missingId(position(1, 4)))
                        returns(1, position(1, 7))
                    }
                },
                error = position(1, 4))
    }

    @Test
    fun `reports missing parenthesis from argument list`() {
        assertModel(
                code = "def a 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        returns(missingExpression())
                    }
                },
                error = position(1, 6))
        assertModel(
                code = "def a x 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns(1, position(1, 8))
                    }
                },
                errors = positions {
                    position(1, 6)
                    position(1, 8)
                })
        assertModel(
                code = "def a x, y 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        argument("y", position(1, 9))
                        returns(missingExpression())
                    }
                },
                errors = positions {
                    position(1, 6)
                    position(1, 11)
                })
    }

    @Test
    fun `reports missing comma in argument list`() {
        assertModel(
                code = "def a(x y) 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns(1, position(1, 11))
                    }
                },
                error = position(1, 8))
    }

    @Test
    fun `reports extra comma after last argument`() {
        assertModel(
                code = "def a(x,) 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        argument(missingId(position(1, 8)))
                        returns(1, position(1, 10))
                    }
                },
                error = position(1, 8))
        assertModel(
                code = "def a(x, y,) 1",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        argument("y", position(1, 9))
                        argument(missingId(position(1, 11)))
                        returns(1, position(1, 13))
                    }
                },
                error = position(1, 11))
    }

    @Test
    fun `reports missing expression`() {
        assertModel(
                code = "def a()",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        returns(missingExpression())
                    }
                },
                error = position(1, 7))
        assertModel(
                code = "def a(x, y)",
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        argument("y", position(1, 9))
                        returns(missingExpression())
                    }
                },
                error = position(1, 11))
    }
}
