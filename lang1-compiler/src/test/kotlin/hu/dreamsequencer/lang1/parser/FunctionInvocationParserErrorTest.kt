/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import org.junit.jupiter.api.Test

class FunctionInvocationParserErrorTest {
    @Test
    fun `reports missing name`() {
        assertModel(
                code = "()",
                model = file {},
                error = position(1, 0))
    }

    @Test
    fun `reports missing parenthesis`() {
        assertModel(
                code = "a",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                    }
                },
                error = position(1, 1))
        assertModel(
                code = "a 1",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                    }
                },
                errors = positions {
                    position(1, 2)
                    position(1, 3)
                })
        assertModel(
                code = "a 1, 2",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                        parameter(2, position(1, 5))
                    }
                },
                errors = positions {
                    position(1, 2)
                    position(1, 6)
                })
    }

    @Test
    fun `reports missing comma in parameter list`() {
        assertModel(
                code = "a(1 2)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                    }
                },
                error = position(1, 4))
    }

    @Test
    fun `reports extra comma after last parameter`() {
        assertModel(
                code = "a(1,)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                        parameter(missingExpression())
                    }
                },
                error = position(1, 4))
        assertModel(
                code = "a(1, 2,)",
                model = file {
                    functionInvocation(position(1, 0)) {
                        id("a", position(1, 0))
                        parameter(1, position(1, 2))
                        parameter(2, position(1, 5))
                        parameter(missingExpression())
                    }
                },
                error = position(1, 7))
    }
}
