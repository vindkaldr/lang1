/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

data class ParserError(val line: Int, val characterPosition: Int, val message: String)

fun missingId(position: Position) = Identifier("<missing ID>", position)
fun missingExpression() = EmptyExpression

fun positions(block: PositionsBuilder.() -> Unit) = PositionsBuilder().apply(block).build()

class PositionsBuilder {
    private val positions = mutableListOf<Position>()

    fun position(line: Int, characterPosition: Int) {
        positions += Position(line, characterPosition)
    }

    fun build() = positions
}
