/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import org.junit.jupiter.api.Test

class IfExpressionConditionParserTest {
    @Test
    fun `parses if expression with equal condition`() {
        assertModel(
                code = """
                    |def a(x)
                    |    if x == 0
                    |        0
                    |    else
                    |        1
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left("x", position(2, 7))
                                        operator("==")
                                        right(0, position(2, 12))
                                    }
                                }
                                conditionExpression(0, position(3, 8))
                                defaultExpression(1, position(5, 8))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses if expression with lesser than condition`() {
        assertModel(
                code = """
                    |def a(x)
                    |    if x < 0
                    |        0
                    |    else
                    |        1
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left("x", position(2, 7))
                                        operator("<")
                                        right(0, position(2, 11))
                                    }
                                }
                                conditionExpression(0, position(3, 8))
                                defaultExpression(1, position(5, 8))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses if expression with greater than condition`() {
        assertModel(
                code = """
                    |def a(x)
                    |    if x > 0
                    |        0
                    |    else
                    |        1
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left("x", position(2, 7))
                                        operator(">")
                                        right(0, position(2, 11))
                                    }
                                }
                                conditionExpression(0, position(3, 8))
                                defaultExpression(1, position(5, 8))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses if expression with function invocation in condition`() {
        assertModel(
                code = """
                    |def a(x)
                    |    if b(x + 1) == 0
                    |        0
                    |    else
                    |        1
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left {
                                            functionInvocation(position(2, 7)) {
                                                id("b", position(2, 7))
                                                parameter {
                                                    binaryExpression(position(2, 9)) {
                                                        left("x", position(2, 9))
                                                        operator("+")
                                                        right(1, position(2, 13))
                                                    }
                                                }
                                            }
                                        }
                                        operator("==")
                                        right(0, position(2, 19))
                                    }
                                }
                                conditionExpression(0, position(3, 8))
                                defaultExpression(1, position(5, 8))
                            }
                        }
                    }
                })
    }

    @Test
    fun `parses if expression with binary expression in condition`() {
        assertModel(
                code = """
                    |def a(x, y)
                    |    if x == y + 1
                    |        0
                    |    else
                    |        1
                """.trimMargin(),
                model = file {
                    functionDefinition(position(1, 0)) {
                        id("a", position(1, 4))
                        argument("x", position(1, 6))
                        argument("y", position(1, 9))
                        returns {
                            ifExpression(position(2, 4)) {
                                condition {
                                    binaryExpression(position(2, 7)) {
                                        left("x", position(2, 7))
                                        operator("==")
                                        right {
                                            binaryExpression(position(2, 12)) {
                                                left("y", position(2, 12))
                                                operator("+")
                                                right(1, position(2, 16))
                                            }
                                        }
                                    }
                                }
                                conditionExpression(0, position(3, 8))
                                defaultExpression(1, position(5, 8))
                            }
                        }
                    }
                })
    }
}
