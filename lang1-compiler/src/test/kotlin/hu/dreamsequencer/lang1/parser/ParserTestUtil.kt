/*
 *     Copyright (C) 2017  Mihály Szabó
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package hu.dreamsequencer.lang1.parser

import hu.dreamsequencer.lang1.Lang1Lexer
import hu.dreamsequencer.lang1.Lang1Parser
import hu.dreamsequencer.lang1.parser.visitor.FileVisitor
import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.BaseErrorListener
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import java.io.StringReader

fun assertModel(code: String, model: File, error: Position) {
    assertModel(code, model, listOf(error))
}

fun assertModel(code: String, model: File, errors: List<Position> = emptyList()) {
    val configuration = getParserConfiguration(code)
    assertThat(getModel(configuration.parser), equalTo(model))
    assertThat(configuration.errors, equalTo(errors))
}

private fun getParserConfiguration(code: String): ParserConfiguration {
    val errorListener = getParserErrorListener()
    val parser = getParser(code, errorListener)
    return ParserConfiguration(parser, errorListener)
}

private fun getParserErrorListener() = ParserErrorListener()

private fun getParser(code: String, errorListener: ParserErrorListener) :Lang1Parser {
    return Lang1Parser(CommonTokenStream(getLexer(code))).apply {
        removeErrorListeners()
        addErrorListener(errorListener)
    }
}
private fun getLexer(code: String) = Lang1Lexer(ANTLRInputStream(StringReader(code)))

private fun getModel(parser: Lang1Parser) = FileVisitor().visit(parser.file())

private class ParserConfiguration(val parser: Lang1Parser, private val errorListener: ParserErrorListener) {
    val errors: List<Position>
        get() =  errorListener.errors.map { Position(it.line, it.characterPosition) }
}

private class ParserErrorListener : BaseErrorListener() {
    val errors = mutableListOf<ParserError>()

    override fun syntaxError(recognizer: Recognizer<*, *>?, offendingSymbol: Any?,
            line: Int, charPositionInLine: Int, msg: String,
            e: RecognitionException?) {
        requireNotNull(line)
        requireNotNull(charPositionInLine)
        requireNotNull(msg)
        errors += ParserError(line, charPositionInLine, msg)
    }
}
